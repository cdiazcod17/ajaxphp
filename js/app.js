$(function () {
    console.log('jquery is working');
    $('#resultado-tarea').hide();
    fetchTarea();
    let edit = false;

    $('#search').keyup(function () { //buscamos el input a traves de su id
        if ($('#search').val()) {
            let search = $('#search').val(); //asignamos a una variable su valor
            $.ajax({ //metodo de jquery para hacer peticion a servidor y hacerlo un objeto
                url: 'buscar-tarea.php',
                data: {
                    search
                },
                type: 'POST',
                success: function (response) {
                    let tareas = JSON.parse(response); //convertir a formato JSON
                    let template = '';

                    tareas.forEach(tarea => {
                        template += `<li><a href="#" class="item-tarea">
                        ${tarea.nombre}</a>
                        </li>` //colocar un valor
                    });                    
                    $('#resultado-tarea').show();
                    $('#container').html(template);
                }
            });
        }
    });

    $('#tarea-form').submit(function (e) { //funcion para recibir los datos por post con ajax
        const postData = {
            nombre: $('#nombre').val(),
            descripcion: $('#descripcion').val(),   
            id:$('#tareaId').val()
        };
        let url = edit === false ? 'agregar-tarea.php': 'editar-tarea.php';
        //metodo de ajax para enviar la informacion 1.a donde envio la informacion 2. que informacion envio3
        //funcion
        $.post('agregar-tarea.php', postData, function (response) {
            edit = false;
            fetchTarea();
            $('#tarea-form').trigger('reset'); //resetear formulario
        });
        e.preventDefault(); //evento para que no refresque pagina 
    });

    function fetchTarea() {
        $.ajax({
            url: 'lista-tareas.php',
            type: 'GET',
            success: function (response) {
                let tarea = JSON.parse(response);
                let template = '';
                tarea.forEach(tarea => { //la clase borrar-tarea es para el evento con javaScript
                    template += `<tr idTarea="${tarea.id}"   >
                <td  ${tarea.id}> ${tarea.id} </td>
                <td> <a class="item-tarea">${tarea.nombre}</a></td>
                <td> ${tarea.descripcion} </td>
                <td> <button class="borrar-tarea btn btn-danger">Delete</button> </td> 
                </tr>`
                });
                $('#tareas-tabla').html(template);
            }
        });
    }

    $(document).on('click', '.borrar-tarea', function () { //con jquery llamamos el evento .borrar-tarea
        if (confirm('Estas seguro de querer elminar tarea?')) {
            let elemento = $(this)[0].parentElement.parentElement; //parentElement me sirve para selecionar el elemnto padre que contiene el boton
            let id = $(elemento).attr('idTarea');
            $.post('borrar_tarea.php', {
                id
            }, function (response) {
                fetchTarea();
            }) //uso el elemento post de jquery para enviarlo al back
        }

    });
    $(document).on('click','.item-tarea',function(){
        let elemento= $(this)[0].parentElement.parentElement;
        let id = $(elemento).attr('idTarea');
        $.post('select-tarea.php',{id},function(response) {
            const tarea =JSON.parse(response);
            $('#nombre').val(tarea.nombre);
            $('#descripcion').val(tarea.descripcion);
            $('#tareaId').val(tarea.id);
            edit = true;
        })
    });
});