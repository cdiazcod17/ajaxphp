<?php 
include  'db/db.php';
$query = "SELECT * from tareas";
$resultado = mysqli_query($conexion, $query);
if(!$resultado) {
  die('Query Failed'. mysqli_error($conexion));
}

$json = array();//variable json para crear los objetos y mostrarlos
while($row = mysqli_fetch_array($resultado)) {
  $json[] = array(
    'nombre' => $row['nombre'],
    'descripcion' => $row['descripcion'],
    'id' => $row['id']
  );
}
$jsonstring = json_encode($json);//decodificarla a json
echo $jsonstring;
?>