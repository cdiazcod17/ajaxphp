<?php

include('db/db.php');

if(isset($_POST['id'])) {
  $id = mysqli_real_escape_string($conexion, $_POST['id']);

  $query = "SELECT * from tareas WHERE id = {$id}";

  $result = mysqli_query($conexion, $query);
  if(!$result) {
    die('Query Failed'. mysqli_error($conexion));
  }

  $json = array();
  while($row = mysqli_fetch_array($result)) {
    $json[] = array(
      'nombre' => $row['nombre'],
      'descripcion' => $row['descripcion'],
      'id' => $row['id']
    );
  }
  $jsonstring = json_encode($json[0]);
  echo $jsonstring;
}

?>
